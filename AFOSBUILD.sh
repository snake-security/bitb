rm -rf /opt/ANDRAX/bitb

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

cp -Rf $(pwd) /opt/ANDRAX/bitb

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
